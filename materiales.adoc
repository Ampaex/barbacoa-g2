== Lista de materiales

// Materiales necesarios por categoría, ordenados alfabéticamente

=== Cocina y comida

* Cucharas
* Cucharillas
* Cucharillas largas para remover
* Cuchillos
* Hielo
* Papel aluminio para asar
* Platos
* Servilletas
* Tenedores
* Vasos
* Vasos de chupito

=== Mobiliario

* Hamaca
* Mesas
* Mesa supletoria para juegos
* Parrilla
* Sillas
* Sombrillas

=== Diversión

* Altavoces
* Balón de fútbol
* Balón de rugby
* Baraja española de cartas
* Dados de rol
* Pañuelo
* Party and Co. (juego de mesa)
* Radiocassette
* Slack line
* Soga
